<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style type="text/css">
	tr{
		 align="center";
	}
</style>
<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
	$(function(){
		//给省加载数据
		$.getJSON("AllServlet?method=selectCity","",function(data){
			for(var i=0;i<data.length;i++){
				var node="<option value='"+data[i].location_id+"'>"+data[i].city+"</option>";
				$("#location").append(node);
			}
		});
		//下拉列表-省的change事件
		$("#location").change(function(){
			//先获取省的id
			var lid=$(this).val();
			var param={"lid":lid};
			$.getJSON("AllServlet?method=selectdepByid",param,function(data){
				$("#department option:gt(0)").remove();//除了请选择外，移除其他的列表项
				for(var i=0;i<data.length;i++){
					var node="<option value='"+data[i].department_id+"'>"+data[i].department_name+"</option>";
					$("#department").append(node);
				}
			});
		});
		//下拉列表-部门的change事件
		$("#department").change(function(){
			//先获取省的id
			var did=$(this).val();
			var param={"did":did};
			$.getJSON("AllServlet?method=selectempByid",param,function(data){
				$("#employee option:gt(0)").remove();//除了请选择外，移除其他的列表项
				for(var i=0;i<data.length;i++){
					var node="<option value='"+data[i].employee_id+"'>"+data[i].last_name+"</option>";
					$("#employee").append(node);
				}
			});
		});
		//动态生成表格
		 $("#btn").click(function(){
			$.getJSON("AllServlet?method=selectCity","",function(data){
				var tt="<tr><th>省id</th><th>省名称</th></tr>";
				$("#btable").append(tt);
				for(var i=0;i<data.length;i++){
					var node="<tr  align='center'><td>"+data[i].location_id+"</td><td>"+data[i].city+"</td></tr>";
					$("#btable").append(node);
				}
			});
		});
	});
</script>
</head>
<body>
	请选择省:
	<select id="location">
		<option value="">请选择</option>
	</select>
	请选择部门:
	<select id="department">
		<option value="">请选择</option>
	</select>
	请选择人员:
	<select id="employee">
		<option value="">请选择</option>
	</select>
	<input type="button" id="btn" value="动态生成表格"/>
	<table align="center" width="500" cellspacing="0" cellpadding="0" id="btable">
		
		
	</table>	
</body>
</html>