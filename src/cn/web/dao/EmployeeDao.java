package cn.web.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import cn.web.Tool.C3P0Conn;
import cn.web.entity.Employee;

public class EmployeeDao {
	private static Connection conn = null;
	private QueryRunner qr = new QueryRunner();
	/**
	 * 查询所有员工信息
	 * @return
	 */
		public List<Employee>  selectempAll(int id) {
			List<Employee> list=new ArrayList<Employee>();
			conn=C3P0Conn.getconn();
			String sql="select * from employees where department_id=?"; 
			try {
				list=qr.query(conn, sql, new BeanListHandler<Employee>(Employee.class),id);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				C3P0Conn.closeAll(conn);
			}
			return list;	
		}	
		public static void main(String[] args) {
			EmployeeDao dao= new EmployeeDao();
			List<Employee> list = dao.selectempAll(1);
			System.out.println(list);
		}
}
