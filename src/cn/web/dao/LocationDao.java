package cn.web.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import cn.web.Tool.C3P0Conn;
import cn.web.entity.Location;

public class LocationDao {
	private static Connection conn = null;
	private QueryRunner qr = new QueryRunner();
	/**
	 * 查询所有城市信息
	 * @return
	 */
		public List<Location>  selectcityAll() {
			List<Location> list=new ArrayList<Location>();
			conn=C3P0Conn.getconn();
			String sql="select * from locations"; 
			try {
				list=qr.query(conn, sql, new BeanListHandler<Location>(Location.class));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				C3P0Conn.closeAll(conn);
			}
			return list;	
		}	
		public static void main(String[] args) {
			LocationDao locationDao=new LocationDao();
			List<Location> list= locationDao.selectcityAll();
			System.out.println(list);
		}
}
