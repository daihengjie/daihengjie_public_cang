package cn.web.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import cn.web.Tool.C3P0Conn;
import cn.web.entity.Department;


public class DepartmentDao {
	private static Connection conn = null;
	private QueryRunner qr = new QueryRunner();
	/**
	 * 查询所有部门信息
	 * @return
	 */
		public List<Department>  selectdepAll(int id) {
			List<Department> list=new ArrayList<Department>();
			conn=C3P0Conn.getconn();
			String sql="select * from departments where location_id=?"; 
			try {
				list=qr.query(conn, sql, new BeanListHandler<Department>(Department.class),id);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				C3P0Conn.closeAll(conn);
			}
			return list;	
		}	
		public static void main(String[] args) {
			DepartmentDao dao= new DepartmentDao();
			List<Department> list= dao.selectdepAll(1);
			System.out.println(list);
		}
}
