package cn.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import cn.web.dao.DepartmentDao;
import cn.web.dao.EmployeeDao;
import cn.web.dao.LocationDao;
import cn.web.entity.Department;
import cn.web.entity.Employee;
import cn.web.entity.Location;

/**
 * Servlet implementation class AllServlet
 */
@WebServlet("/AllServlet")
public class AllServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AllServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String mname=request.getParameter("method");
		try {
			Method method=this.getClass().getDeclaredMethod(mname, HttpServletRequest.class,HttpServletResponse.class);
			method.invoke(this, request,response);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	protected void selectCity(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
		LocationDao lDao = new LocationDao();
		List<Location> list= lDao.selectcityAll();
		System.out.println(list);
		ObjectMapper mapper=new ObjectMapper();
		String json=mapper.writeValueAsString(list);
		System.out.println(json);
		PrintWriter out=response.getWriter();
		out.print(json);//返回json数据
	}
	protected void selectdepByid(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
		DepartmentDao dao= new DepartmentDao();
		String sid=request.getParameter("lid");
		if(null!=sid&&!"".equals(sid)){
			int id=Integer.parseInt(sid);
			List<Department> list=dao.selectdepAll(id);
			System.out.println(list);
			ObjectMapper mapper = new ObjectMapper();
			String json= mapper.writeValueAsString(list);
			System.out.println(json);
			PrintWriter out = response.getWriter();
			out.print(json);
		}
	}
	protected void selectempByid(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json;charset=UTF-8");
		EmployeeDao dao = new EmployeeDao();
		String sid=request.getParameter("did");
		if(null!=sid&&!"".equals(sid)){
			int id=Integer.parseInt(sid);
			List<Employee> list=dao.selectempAll(id);
			System.out.println(list);
			ObjectMapper mapper = new ObjectMapper();
			String json= mapper.writeValueAsString(list);
			System.out.println(json);
			PrintWriter out = response.getWriter();
			out.print(json);
		}else {
			System.out.println(111);
		}
	}
}
