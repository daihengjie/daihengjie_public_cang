package cn.web.entity;

public class Location {
	private int location_id;
	private String city;
	private String street_address;
	private String postal_code;
	private String state_province;
	private String country_id;
	
	public Location() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Location(int location_id, String city, String street_address, String postal_code, String state_province,
			String country_id) {
		super();
		this.location_id = location_id;
		this.city = city;
		this.street_address = street_address;
		this.postal_code = postal_code;
		this.state_province = state_province;
		this.country_id = country_id;
	}

	public int getLocation_id() {
		return location_id;
	}

	public void setLocation_id(int location_id) {
		this.location_id = location_id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet_address() {
		return street_address;
	}

	public void setStreet_address(String street_address) {
		this.street_address = street_address;
	}

	public String getPostal_code() {
		return postal_code;
	}

	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
	}

	public String getState_province() {
		return state_province;
	}

	public void setState_province(String state_province) {
		this.state_province = state_province;
	}

	public String getCountry_id() {
		return country_id;
	}

	public void setCountry_id(String country_id) {
		this.country_id = country_id;
	}

	@Override
	public String toString() {
		return "Location [location_id=" + location_id + ", city=" + city + ", street_address=" + street_address
				+ ", postal_code=" + postal_code + ", state_province=" + state_province + ", country_id=" + country_id
				+ "]";
	}
	
	
	
}
